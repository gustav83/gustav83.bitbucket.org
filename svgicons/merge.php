#!/usr/bin/php
<?php

echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" style=\"display:none\">\n";

for($i = 1; $i < $argc; $i++) {
    $file = $argv[$i];
    $name = pathinfo($file, PATHINFO_FILENAME);
    $dom = new DOMDocument();
    $dom->load($file);
    $svg = $dom->getElementsByTagName('svg')[0];
    $width = $svg->getAttribute('width');
    $height = $svg->getAttribute('height');
    $viewBox = $svg->getAttribute('viewBox');
    echo "<symbol id=\"$name\" viewBox=\"$viewBox\">\n";
    foreach($svg->childNodes as $node) {
        if($node instanceof DOMElement) {
            $node->removeAttribute('fill');
            $node->removeAttribute('stroke');
        }
        echo trim($dom->saveHTML($node));
    }
    echo "</symbol>\n";
}

echo "</svg>\n";

# echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"

# for file in $@; do
#     name="${file%.*}"
#     echo "<symbol id=\"$name\" class=\"symbol-$name\" viewBox=\"0 0 32 32\">"
#     grep -v "<svg" "$file" | grep -v "/svg>" | grep -v "<!--" | sed -e 's/\(fill\)="[^"]*"//' | sed -e 's/\(stroke\)="[^"]*"//'
#     echo "</symbol>"
# done

# echo "</svg>"
