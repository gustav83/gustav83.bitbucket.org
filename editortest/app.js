import Vue from 'vue'
import App from'./editor.vue'
import Sortable from 'sortablejs'

Vue.directive('sortable', {
  inserted: function (el, binding) {
    new Sortable(el, binding.value || {})
  }
})

import { VueEditor } from 'vue2-quill-editor'
/*Vue.use(VueEditor)*/

/*import VueQuillEditor from 'vue-quill-editor/ssr'
Vue.use(VueQuillEditor)*/

/*import VueTinymce from 'vue-tinymce'
Vue.use(VueTinymce)*/

//VueTinymce = require('vue-tinymce')
//Vue.use(VueTinymce)

/*import TinyMCE from 'tinymce-vue-2';
//var TinyMCE = require('tinymce-vue-2')
console.log("poo")
console.log(TinyMCE.TinyMCE);
Vue.component('tinymce', TinyMCE.TinyMCE);*/

new Vue({
  el: '#app',
  render: function (createElement) {
    App.data.msg = "poo"
    console.log(App)
    var el = createElement(App, {
      props: {
        options: "poo"
      }
    })
    console.log(el)
    return el
  }
})
