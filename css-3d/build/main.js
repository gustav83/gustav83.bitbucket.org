"use strict";

function click(t) {
    var e = parseInt(t.getAttribute("counter")), a = parseInt(t.getAttribute("maxcount")), o = e ? e + 1 : 1;
    a && o > a && (o = 0), t.setAttribute("counter", o), t.classList.toggle("clicked");
}

function init() {
    elements = {
        root: document.getElementById("root")
    }, $("#root > div").each(function(t) {
        $(this).addClass("object-" + (t + 1)), $("*", this).addClass("object-" + (t + 1));
    }), $(document).on("keydown", function(t) {
        debug("event", t, 10, !0), debug("keydown", t.key, 5), inputState[t.key] = !0;
    }), $(document).on("keyup", function(t) {
        debug("event", t, 10, !0), debug("keyup", t.key, 5), inputState[t.key] = !1;
    }), $(document).on("click", ".face", function(t) {
        click(t.currentTarget), click(t.currentTarget.parentNode);
    }), requestAnimationFrame(update), setInterval(updateDebug, 500);
}

function clamp(t, e, a) {
    return t <= e ? e : t >= a ? a : t;
}

function update(t) {
    timer || (timer = t);
    var e = t - timer;
    timer = t, updatePlayer(e), updateTransform(e), requestAnimationFrame(update);
}

function updatePlayer(t) {
    var e = .05 * t;
    inputState.d && (playerState.position[0] += e), inputState.a && (playerState.position[0] -= e), 
    inputState.w && (playerState.position[2] += e), inputState.s && (playerState.position[2] -= e), 
    inputState.ArrowUp && (inputState.Shift ? playerState.rotation[0] += e : playerState.position[1] += e), 
    inputState.ArrowDown && (inputState.Shift ? playerState.rotation[0] -= e : playerState.position[1] -= e), 
    inputState.ArrowLeft && (inputState.Shift ? playerState.rotation[2] -= e : playerState.rotation[1] -= e), 
    inputState.ArrowRight && (inputState.Shift ? playerState.rotation[2] += e : playerState.rotation[1] += e);
    for (var a = 0; a < 3; ++a) playerState.velocity[a] = clamp(playerState.velocity[a], -options.maxVelocity, options.maxVelocity), 
    playerState.position[a] += playerState.velocity[a];
    debug("playerState", window.playerState);
}

function updateTransform(t) {
    var e = "rotateX({rx}deg) rotateY({ry}deg) rotateZ({rz}deg) translate3d({px}em, {py}em, {pz}em)";
    e = e.replace("{px}", window.playerState.position[0]), e = e.replace("{py}", window.playerState.position[1]), 
    e = e.replace("{pz}", window.playerState.position[2]), e = e.replace("{rx}", window.playerState.rotation[0]), 
    e = e.replace("{ry}", window.playerState.rotation[1]), e = e.replace("{rz}", window.playerState.rotation[2]), 
    elements.root.style.transform = e, debug("transform", e);
}

function updateDebug() {
    $("#debug").text("DEBUG = " + JSON.stringify(debugData, null, 1));
}

function debug(t, e) {
    var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0, o = arguments.length > 3 && void 0 !== arguments[3] && arguments[3], n = o ? debugDataHidden : debugData;
    a ? (n[t] || (n[t] = []), n[t].unshift(e), n[t] = n[t].slice(0, a)) : n[t] = e;
}

var $ = jQuery;

window.log = console.log, window.elements = null, window.inputState = {}, window.debugData = {}, 
window.debugDataHidden = {}, window.playerState = {
    position: [ 6.5, -60, 0 ],
    velocity: [ 0, 0, 0 ],
    rotation: [ 0, 0, 0 ]
}, window.options = {
    maxVelocity: 2
}, window.timer = null, $(function() {
    init();
});
//# sourceMappingURL=main.js.map
