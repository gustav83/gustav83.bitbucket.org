
// Config
var themePath = '.';
var port = 8001;

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compact'
};

var minifycssOptions = {
  keepBreaks: true
};

var uglifyOptions = {
  output: {
    beautify: true
  }
};

var babelOptions = {
  presets: ['es2015'],
}

var autoprefixerOptions = {
  browsers: ['> 1%', 'IE 8', 'IE 9', 'Firefox > 10', 'iOS > 6'],
  cascade: false,
};

// Autoprefixer fix
require('es6-promise').polyfill();

// Init
var gulp = require('gulp'); 
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var typescript = require('gulp-typescript');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');;
var uglify = require('gulp-uglify');
var run = require('gulp-run');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var extend = require('extend');
var concat = require('gulp-concat');
var fs = require('fs');

var config = {}
var defaults = {
  phpServer: {
    enable: true,
    port: port,
  },
  browserSync: {
    open: false,
    port: 3000,
    proxy: 'localhost:' + port,
    files: [
      themePath + '/build/**/*.js',
      themePath + '/build/**/*.css',
      '**/*.php',
      '**/*.html',
    ]
  }
}

if(fs.existsSync('.gulp.json')) {
  config = require('.gulp.json');
}

config = extend(true, defaults, config);

// Error handler
function errorHandler(error) {
  console.log(error.toString());
  this.emit('end');
}

// CSS
gulp.task('build-sass', function() {
  return gulp.src(themePath + '/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions)).on('error', sass.logError)
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(minifycss(minifycssOptions))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themePath + '/build'));
});

// ES6
gulp.task('build-es6', function () {
  return gulp.src(themePath + '/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel(babelOptions)).on('error', errorHandler)
    .pipe(uglify(uglifyOptions))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(themePath + '/build'));
});

gulp.task('build', function(callback) {
  return runSequence(
    'build-sass',
    ['build-es6'],
    callback
  );
});

// Browsersync
gulp.task('serve', ['build'], function(done) {
  if(config.phpServer.enable) {
    run('php -S localhost:' + config.phpServer.port + ' -t ./ > php.log 2>&1').exec()
  }
  browserSync(config.browserSync, done);
});

// Watch files
gulp.task('watch', ['serve'], function() {
  gulp.watch(themePath + '/sass/**/*.scss', ['build-sass']);
  gulp.watch(themePath + '/js/**/*.js', ['build-es6']);
});

// Default task
gulp.task('default', ['build-sass', 'build-es6', 'watch']);
