
let $ = jQuery
window.log = console.log
window.elements = null
window.inputState = {}
window.debugData = {}
window.debugDataHidden = {}
window.playerState = {
  position: [6.5, -60, 0],
  velocity: [0, 0, 0],
  rotation: [0, 0, 0],
}
window.options = {
  maxVelocity: 2.0,
}
window.timer = null

function click(element) {
  let counter = parseInt(element.getAttribute('counter'))
  let maxcount = parseInt(element.getAttribute('maxcount'))
  let newcount = counter ? counter + 1 : 1
  if(maxcount && newcount > maxcount) {
    newcount = 0
  }
  element.setAttribute('counter', newcount)
  element.classList.toggle('clicked')
}

function init() {
  elements = {
    root: document.getElementById('root'),
  }

  $('#root > div').each(function(i) {
    $(this).addClass('object-' + (i + 1))
    $('*', this).addClass('object-' + (i + 1))
  })

  $(document).on('keydown', event => {
    debug('event', event, 10, true)
    debug('keydown', event.key, 5)
    inputState[event.key] = true
  })

  $(document).on('keyup', event => {
    debug('event', event, 10, true)
    debug('keyup', event.key, 5)
    inputState[event.key] = false
  })

  $(document).on('click', '.face', event => {
    click(event.currentTarget)
    click(event.currentTarget.parentNode)
  })

  requestAnimationFrame(update)
  setInterval(updateDebug, 500)
}

function clamp(num, min, max) {
  return num <= min ? min : num >= max ? max : num
}

function update(ts) {
  if(!timer) timer = ts
  let delta = ts - timer

  timer = ts

  updatePlayer(delta)
  updateTransform(delta)

  requestAnimationFrame(update)
}

function updatePlayer(delta) {
  let speed = delta * 0.05

  if(inputState['d']) {
    playerState.position[0] += speed
  }

  if(inputState['a']) {
    playerState.position[0] -= speed
  }

  if(inputState['w']) {
    playerState.position[2] += speed
  }

  if(inputState['s']) {
    playerState.position[2] -= speed
  }

  if(inputState['ArrowUp']) {
    if(inputState['Shift']) {
      playerState.rotation[0] += speed
    } else {
      playerState.position[1] += speed
    }
  }

  if(inputState['ArrowDown']) {
    if(inputState['Shift']) {
      playerState.rotation[0] -= speed
    } else {
      playerState.position[1] -= speed
    }
  }

  if(inputState['ArrowLeft']) {
    if(inputState['Shift']) {
      playerState.rotation[2] -= speed
    } else {
      playerState.rotation[1] -= speed
    }
  }

  if(inputState['ArrowRight']) {
    if(inputState['Shift']) {
      playerState.rotation[2] += speed
    } else {
      playerState.rotation[1] += speed
    }
  }

  for(let i = 0; i < 3; ++i) {
    playerState.velocity[i] = clamp(playerState.velocity[i], -options.maxVelocity, options.maxVelocity)
    playerState.position[i] += playerState.velocity[i]
  }
  debug('playerState', window.playerState)
}

function updateTransform(delta) {
  let transform =
      'rotateX({rx}deg) rotateY({ry}deg) rotateZ({rz}deg) ' +
      'translate3d({px}em, {py}em, {pz}em)'
  transform = transform.replace('{px}', window.playerState.position[0])
  transform = transform.replace('{py}', window.playerState.position[1])
  transform = transform.replace('{pz}', window.playerState.position[2])
  transform = transform.replace('{rx}', window.playerState.rotation[0])
  transform = transform.replace('{ry}', window.playerState.rotation[1])
  transform = transform.replace('{rz}', window.playerState.rotation[2])
  elements.root.style.transform = transform
  debug('transform', transform)
}

function updateDebug() {
  $('#debug').text('DEBUG = ' + JSON.stringify(debugData, null, 1))
}

function debug(key, value, history = 0, hidden = false) {
  let data = hidden ? debugDataHidden : debugData
  if(history) {
    if(!data[key]) {
      data[key] = []
    }
    data[key].unshift(value)
    data[key] = data[key].slice(0, history)
  } else {
    data[key] = value
  }
}

$(() => {
  init()
})
